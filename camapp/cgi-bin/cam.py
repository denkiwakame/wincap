#!/usr/bin/python
import sys, os
import json, cgi

query_params = cgi.FieldStorage()

mode = int(query_params['mode'].value) if query_params.has_key('mode') else -1
pattern_num = int(query_params['pattern_num'].value) if query_params.has_key('pattern_num') else -1
x_split_num = query_params['x_split_num'].value if query_params.has_key('x_split_num') else -1
y_split_num = query_params['y_split_num'].value if query_params.has_key('y_split_num') else -1
do_capture = int(query_params['capture'].value) if query_params.has_key('capture') else 0

def gen_fpath(mode, pattern_num, fext):
    if int(pattern_num) < 10 :
        num_str = "00" + str(pattern_num)
    elif int(pattern_num) < 100 :
        num_str = "0" + str(pattern_num)
    else :
        num_str = str(pattern_num)

    return str(mode) + "_" + num_str + fext

if do_capture:
  sys.stderr.write("capturing...\n")
  #os.system("./cgi-bin/FlyCapture2Test/FlyCapture2Test " + gen_fpath(mode, pattern_num, ".png"))
else:
  os.system("sleep 1")

if mode == 1:
    mode = 2 if pattern_num == x_split_num else 1
elif mode == 2:
    mode = 0 if pattern_num == x_split_num + y_split_num else 2

res = json.dumps({
                    'next_mode': mode,
                    'next_pattern_num': int(pattern_num)+1,
                    })
# response
print "Content-type: application/json\n"
print res
