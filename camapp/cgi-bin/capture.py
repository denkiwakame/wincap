#!/usr/bin/python
import sys, os
import json, cgi

query_params = cgi.FieldStorage()

# common
fname           = str(query_params['fname'].value) if query_params.has_key('fname') else 'preview'
fext            = str(query_params['fext'].value) if query_params.has_key('fext') else '.png'
capture_mode    = int(query_params['capture'].value) if query_params.has_key('capture') else 0

# preview
sec             = str(query_params['persec'].value) if query_params.has_key('persec') else "1"

# graycode, chessboard
dir_name        = str(query_params['dir_name'].value) if query_params.has_key('dir_name') else ''

# graycode, preview
cam_num         = int(query_params['cam_num'].value) if query_params.has_key('cam_num') else 0
# graycode
set_num         = int(query_params['set_num'].value) if query_params.has_key('set_num') else 0
shutter_speeds  = int(query_params['shutter_speeds'].value) if query_params.has_key('shutter_speeds') else 0

root_dir = "./data/" + dir_name

# CAPTURE COMMAND
if capture_mode:
    # GRAYCODE MODE
    if set_num:

        # prepare dirs
        for cnum in range(cam_num+1):
            cam_set_dir = "cam" + str(cnum) + "/set" + str(set_num) + "/"
            os.system("mkdir -p " + root_dir + cam_set_dir)

            #shutters = [30.0, 10.0, 50.0]
            shutters = [30.0]
            shutter_idx = 0
            for shutter in shutters:

                fpath = root_dir + cam_set_dir + fname + "_" + str(shutter_idx) + fext
                cmd = "./cgi-bin/FLYCAP/aqua/bin/capture/capture -c " + str(cnum) + " -n 1 --rows 964 --cols 1296 --top 0 --left 0 -f 10.0 --shutter " + str(shutter) + " -o " + fpath

                sys.stderr.write(cmd)
                os.system(cmd)
                shutter_idx = shutter_idx+1

    # PREVIEW MODE OR INCALIB MODE
    else:
        #os.system("./cgi-bin/easyCapture/easycap " + fpath)
        # PREVIEW MODE
        if fname == "preview":
            fpath = root_dir + fname
            for cnum in range(cam_num+1):
                fpath = root_dir + fname + str(cnum) + fext
                cmd = "./cgi-bin/FLYCAP/aqua/bin/capture/capture -c " + str(cnum) + " -n 1 --rows 964 --cols 1296 --top 0 --left 0 -o " + fpath
                sys.stderr.write(cmd)
                os.system(cmd)
        # INCALIB MODE
        else:
            fpath = root_dir + fname + fext
            cmd = "./cgi-bin/FLYCAP/aqua/bin/capture/capture -c " + str(cam_num) + " -n 1 --rows 964 --cols 1296 --top 0 --left 0 -f 10.0 --shutter 10 -o " + fpath
            sys.stderr.write(cmd)
            os.system(cmd)
# DEMO MODE
else:
    os.system("sleep " + sec)

res = json.dumps({
                    'fpath' : fpath,
                    'status': "SUCCESS",
                    'cmd'   : cmd,
                    })

print "Content-type: application/json\n"
print res
