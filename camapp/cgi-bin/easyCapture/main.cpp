#include "FlyCapture2.h"
#include <math.h>

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <vector>

static const int SHUTTER_SPEED_DEFAULT = 5;

void PrintBuildInfo();
void PrintCameraInfo( FlyCapture2::CameraInfo* pCamInfo );
int CaptureImage(   FlyCapture2::Camera &cam, FlyCapture2::CameraInfo &camInfo, std::string& filename );
void PrintError( FlyCapture2::Error error ) { error.PrintErrorTrace(); }
int toInt(std::string s) { int r = 0; std::istringstream ss(s); ss >> r; return r; }
float toFloat(std::string s) { float r = 0.0; std::istringstream ss(s); ss >> r; return (float)r; }
std::vector<float> GetShutterSpeeds(int argc, char** argv){
    std::vector<float> shutter_speeds;

    if (argc == 2) {
        shutter_speeds.push_back(SHUTTER_SPEED_DEFAULT);
    } else {
        for(int i=2; i<argc; i++) {
            float spd = toFloat(std::string(argv[i]));
            shutter_speeds.push_back(spd);
        }
    }
    return shutter_speeds;
}
std::vector<std::string> split(const std::string _s, const std::string del)
{
    std::vector<std::string> ret;
    std::string s = _s;
    while (!s.empty())
    {
        size_t pos = s.find(del);
        std::string sub = "";
        sub = s.substr(0, pos);
        ret.push_back(sub);
        if (pos != std::string::npos)
        pos += del.size();
        s.erase(0, pos);
    }
    return ret;
}

int main (int argc, char* argv[]) {
    // PrintBuildInfo();

    if (argc == 1) { std::cerr << "./easycap filename 1 10 100" << std::endl; }
    assert( ( argc != 1 ) && "Usage[1]: ./easycap filename\nUsage[2]: ./easycap filename shutterspeed\n");

    std::string filename = std::string(argv[1]);
    std::vector<float> shutter_speed = GetShutterSpeeds(argc, argv);

    FlyCapture2::Error error;
    FlyCapture2::BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != FlyCapture2::PGRERROR_OK){ PrintError( error ); return -1; }
    std::cerr << "Number of cameras detected: " << numCameras << std::endl;

    // Run Camera
    for (uint i=0; i < numCameras; i++)
    {
        FlyCapture2::PGRGuid guid;
        error = busMgr.GetCameraFromIndex(i, &guid);
        if (error != FlyCapture2::PGRERROR_OK){ PrintError( error ); return -1; }

        FlyCapture2::Error error;
        FlyCapture2::Camera cam;

        // Connect to a camera
        error = cam.Connect(&guid);
        if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1;}

        // Get the camera information
        FlyCapture2::CameraInfo camInfo;
        error = cam.GetCameraInfo(&camInfo);
        if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1;}
        // TODO PrintCameraInfo(&camInfo);

        FlyCapture2::Property prop_frame_rate;
        prop_frame_rate.type = FlyCapture2::FRAME_RATE;
        FlyCapture2::Property prop_shutter;
        prop_shutter.type = FlyCapture2::SHUTTER;
        FlyCapture2::Property prop_exposure;
        prop_exposure.type = FlyCapture2::AUTO_EXPOSURE;

        if (argc >= 3) {
            // Set auto exposure property
            error = cam.GetProperty( &prop_exposure );
            prop_exposure.autoManualMode = false;
            prop_exposure.onOff = false;
            error = cam.SetProperty( &prop_exposure, false);

            // Set framerate property
            error = cam.GetProperty( &prop_frame_rate );
            prop_frame_rate.autoManualMode = false;
            prop_frame_rate.onOff = true;
            prop_frame_rate.absControl=true;
            prop_frame_rate.absValue = (float)10.0;
            error = cam.SetProperty( &prop_frame_rate);

            // Get shutter property
            error = cam.GetProperty( &prop_shutter );
            prop_shutter.autoManualMode = false;
            prop_shutter.onOff = true;
            prop_shutter.absControl = true;
        }

        // Start capturing images
        error = cam.StartCapture();

        // capture images pow(10,n)
        if (argc == 2) {
            CaptureImage( cam, camInfo, filename);
        } else {
            for(uint i=0; i<shutter_speed.size(); i++){

                prop_shutter.absValue = shutter_speed[i];
                error = cam.SetProperty( &prop_shutter, false );
                std::cerr << prop_shutter.absValue << std::endl;
                if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }

                // gen filename
                /*
                std::vector<std::string> fpath = split(filename, ".");
                std::string fext = "." + fpath.back();
                std::vector<std::string> basenames = split(filename, fext);

                std::cerr << basenames[0] << std::endl;
                std::cerr << fext << std::endl;

                std::ostringstream ss;
                ss << basenames[0] << "_" << i << fext;
                std::string shutter_fpath = ss.str();
                std::cerr << shutter_fpath << std::endl;
                */
                //CaptureImage( cam, camInfo, shutter_fpath);
                CaptureImage( cam, camInfo, filename);

            }
        }

        // Stop capturing images
        error = cam.StopCapture();
        if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }

        // Set auto exposure property
        prop_exposure.autoManualMode = true;
        prop_exposure.onOff = true;
        error = cam.SetProperty( &prop_exposure );

        // Set frame rate property
        error = cam.GetProperty( &prop_frame_rate );
        prop_frame_rate.autoManualMode = true;
        prop_frame_rate.onOff = true;
        error = cam.SetProperty( &prop_frame_rate );

        // Get shutter property
        error = cam.GetProperty( &prop_shutter );
        prop_shutter.autoManualMode = true;
        error = cam.SetProperty( &prop_shutter );

        // Disconnect the camera
        error = cam.Disconnect();
        if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }
    }

    return 0;
}

void PrintBuildInfo() {
    FlyCapture2::FC2Version fc2Version;
    FlyCapture2::Utilities::GetLibraryVersion( &fc2Version );
    char version[128];
    sprintf(
        version,
        "FlyCapture2 library version: %d.%d.%d.%d\n",
        fc2Version.major, fc2Version.minor, fc2Version.type, fc2Version.build );

    std::cerr << version << std::endl;

    char timeStamp[512];
    sprintf( timeStamp, "Application build date: %s %s\n\n", __DATE__, __TIME__ );

    std::cerr << timeStamp << std::endl;
}

void PrintCameraInfo( FlyCapture2::CameraInfo* pCamInfo )
{
    printf(
        "\n*** CAMERA INFORMATION ***\n"
        "Serial number - %u\n"
        "Camera model - %s\n"
        "Camera vendor - %s\n"
        "Sensor - %s\n"
        "Resolution - %s\n"
        "Firmware version - %s\n"
        "Firmware build time - %s\n\n",
        pCamInfo->serialNumber,
        pCamInfo->modelName,
        pCamInfo->vendorName,
        pCamInfo->sensorInfo,
        pCamInfo->sensorResolution,
        pCamInfo->firmwareVersion,
        pCamInfo->firmwareBuildTime );
}


int CaptureImage( FlyCapture2::Camera &cam, FlyCapture2::CameraInfo &camInfo, std::string& filename){


    FlyCapture2::Error error;
    FlyCapture2::Image rawImage;
    // Retrieve an image
    error = cam.RetrieveBuffer( &rawImage );
    if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }

    // Create a converted image
    FlyCapture2::Image convertedImage;

    // Convert the raw image
    error = rawImage.Convert( FlyCapture2::PIXEL_FORMAT_MONO8, &convertedImage );
    if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }

    // Create a unique filename
   // char filename[512];
    //sprintf( filename, "FlyCapture2Test-%u-%d.png", camInfo.serialNumber, img_idx);

    // Save the image. If a file format is not passed in, then the file
    // extension is parsed to attempt to determine the file format.
    error = convertedImage.Save( filename.c_str() );
    if (error != FlyCapture2::PGRERROR_OK) { PrintError( error ); return -1; }

    return 0;
}

