#include <iostream>
#include <stdint.h>
#include <inttypes.h>

#include "flycap2_util.h"
#include "boost_opt_util.h"
#include "linux_aio.h"
#include "my_memcpy.h"

struct my_boost_opt_mandatory_check_hook : public boost_opt_mandatory_check_hook_t {
  virtual int before_help_hook(const boost::program_options::variables_map & parameter_map,
                               const boost::program_options::options_description & cmdline,
                               int argc, char * argv[]) {
    if( parameter_map.count("list") == 0 ) {
      return -1;
    }

    FlyCapture2::Error error;
    FlyCapture2::BusManager busMgr;
    unsigned int n;
    error = busMgr.GetNumOfCameras(&n);
    FlyCapture2Util::check_error(error);

    for(unsigned int i=0 ; i<n ; i++) {
      unsigned int serial;
      error = busMgr.GetCameraSerialNumberFromIndex( i, &serial );
      FlyCapture2Util::check_error(error);

      fprintf(stderr, "camera #%d = SN %u\n", i, serial);
    }

    return 0;
  }

  virtual int after_help_hook(const boost::program_options::variables_map & parameter_map,
                              const boost::program_options::options_description & cmdline,
                              int argc, char * argv[]) {
    return -1;
  }
};

int main(int argc, char ** argv) {
  boost::program_options::options_description cmdline("Command line options");
  cmdline.add_options()
    ("help,h", "show help message")
    ("list", "list available cameras")
    ("camera,c",
     boost::program_options::value<unsigned int>(),
     "[MANDATORY] Index or Serial-Number of the camera")
    ("num,n",
     boost::program_options::value<int>(),
     "[MANDATORY] Num of frames to capture")
    ("out,o",
     boost::program_options::value<std::string>()->default_value("/ram/%u"),
     "The output filename of the raw data (%u will be replaced by the serial number)")
    ("fps,f",
     boost::program_options::value<float>()->default_value(15),
     "[MANDATORY] framerate")
    ("shutter",
     boost::program_options::value<float>()->default_value(5),
     "Shutter [ms] (set -1 to keep the current value)")
    ("gain",
     boost::program_options::value<float>()->default_value(10),
     "Gain [dB] (set -1 to keep the current value)")
    ("gamma",
     boost::program_options::value<float>()->default_value(2),
     "Gamma (set -1 to keep the current value)")
    ("wbred",
     boost::program_options::value<float>()->default_value(550),
     "White balance (red) (set -1 to keep the current value)")
    ("wbblue",
     boost::program_options::value<float>()->default_value(810),
     "White balance (blue) (set -1 to keep the current value)")
    ("rows",
     boost::program_options::value<int>()->default_value(960),
     "Image row size (height)")
    ("cols",
     boost::program_options::value<int>()->default_value(1280),
     "Image col size (width)")
    ("left",
     boost::program_options::value<int>()->default_value(8),
     "Image left offset")
    ("top",
     boost::program_options::value<int>()->default_value(4),
     "Image top offset")
    ("trigger,T",
     boost::program_options::value<int>()->default_value(0),
     "Turn on/off trigger mode")
    ("dma_buffers",
     boost::program_options::value<int>()->default_value(12),
     "Num of DMA buffers for grabbing")
    ("io_buffers",
     boost::program_options::value<int>()->default_value(64),
     "Num of buffers for disk writing")
    ("verbose,v",
     "Turn on verbose mode")
    ("nowriting",
     boost::program_options::value<int>()->default_value(0),
     "Turn on/off no-disk-writing mode")
    ("nocapturing",
     boost::program_options::value<int>()->default_value(0),
     "Turn on/off no-capturing mode");

  my_boost_opt_mandatory_check_hook hook;
  boost::program_options::variables_map parameter_map = boost_opt_check(cmdline, argc, argv, &hook);

  const int NFRAMES = parameter_map["num"].as<int>();
  const unsigned int CAMID = parameter_map["camera"].as<unsigned int>();
  const std::string OUT_FNAME = parameter_map["out"].as<std::string>();

  const float ABS_SHUTTER = parameter_map["shutter"].as<float>();
  const float ABS_GAIN = parameter_map["gain"].as<float>();
  const float ABS_GAMMA = parameter_map["gamma"].as<float>();
  const float ABS_FRAME_RATE = parameter_map["fps"].as<float>();
  const float MAN_WB_RED = parameter_map["wbred"].as<float>();
  const float MAN_WB_BLUE = parameter_map["wbblue"].as<float>();

  const int COLS = parameter_map["cols"].as<int>();
  const int ROWS = parameter_map["rows"].as<int>();
  const int LEFT_OFFSET = parameter_map["left"].as<int>();
  const int TOP_OFFSET = parameter_map["top"].as<int>();
  const int TRIGGER = parameter_map["trigger"].as<int>();
  const int VERBOSE = parameter_map.count("verbose");
  const int NUM_BUFFERS = parameter_map["dma_buffers"].as<int>();
  const int DISK_BUFFERS = parameter_map["io_buffers"].as<int>();
  const int NO_WRITING = parameter_map["nowriting"].as<int>();
  const int NO_CAPTURING = parameter_map["nocapturing"].as<int>();
  const int D_ALIGN = 4096;
  const int MEMSIZE = COLS * ROWS;
  const int CYCLE_MAX = 8000;
  const int CYCLE_AVE = CYCLE_MAX / ABS_FRAME_RATE;
  const int CYCLE_TOL = (int)(CYCLE_AVE * 0.05); // 5%�θ����ޤǵ���

  FlyCapture2::Error error;
  FlyCapture2::BusManager busmgr;
  FlyCapture2::Camera camera;
  FlyCapture2::PGRGuid guid;
  FlyCapture2::FC2Config config;

  if(CAMID > 32) {
    error = busmgr.GetCameraFromSerialNumber(CAMID, &guid);
  } else {
    error = busmgr.GetCameraFromIndex(CAMID, &guid);
  }
  FlyCapture2Util::check_error(error);

  error = camera.Connect(&guid);
  FlyCapture2Util::check_error(error);

  FlyCapture2Util::check_firmware(&camera);

  // Stop transmission anyway
  camera.StopCapture();

  // Make sure we get all frames without dropping
  error = camera.GetConfiguration(&config);
  FlyCapture2Util::check_error(error);
  if(config.numBuffers < (unsigned int)NUM_BUFFERS) {
    config.numBuffers = NUM_BUFFERS;
  }
  config.grabMode = FlyCapture2::BUFFER_FRAMES;

  error = camera.SetConfiguration(&config);
  FlyCapture2Util::check_error(error);

  // Turn on embedding
  FlyCapture2Util::enable_info_embedding(&camera);

  // Turn on ABS mode
  FlyCapture2Util::enable_abs_mode(&camera, true);

  if(ABS_SHUTTER >= 0) {
    FlyCapture2Util::set_abs_shutter(&camera, ABS_SHUTTER);
  }
  if(ABS_GAMMA >= 0) {
    FlyCapture2Util::set_abs_gamma(&camera, ABS_GAMMA);
  }
  if(ABS_GAIN >= 0) {
    FlyCapture2Util::set_abs_gain(&camera, ABS_GAIN);
  }
  if(ABS_FRAME_RATE >= 0) {
    FlyCapture2Util::set_abs_frame_rate(&camera, ABS_FRAME_RATE);
  }
  if(MAN_WB_RED >= 0 && MAN_WB_BLUE >= 0) {
    FlyCapture2Util::set_man_white_balance(&camera, MAN_WB_RED, MAN_WB_BLUE);
  }


  // Video mode
  FlyCapture2::Format7ImageSettings fmt7;
  fmt7.mode = FlyCapture2::MODE_0;
  fmt7.offsetX = LEFT_OFFSET;
  fmt7.offsetY = TOP_OFFSET;
  fmt7.width = COLS;
  fmt7.height = ROWS;
  fmt7.pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW8;

  error = camera.SetFormat7Configuration(&fmt7, 100.0f);
  FlyCapture2Util::check_error(error, 1, "SN%u: Cannot set up Format7 mode.\n", CAMID);

  if(NO_CAPTURING) {
    camera.Disconnect();
    return 0;
  }

  // Prepare async output
  libaio::writer_t writer;
  if(! NO_WRITING) {
    char buf[65536];
    snprintf(buf, sizeof(buf), OUT_FNAME.c_str(), CAMID);
    writer.init(buf, MEMSIZE, DISK_BUFFERS, NFRAMES, D_ALIGN);
  }

  // Turn on trigger in
  if(TRIGGER) {
    FlyCapture2Util::set_trigger_in(&camera, true);
  }

  // Turn on trigger out anyway
  //FlyCapture2Util::set_trigger_out(&camera, true);

  // Capture
  error = camera.StartCapture();
  FlyCapture2Util::check_error(error, 1, "SN%u: Error starting camera.\n", CAMID);


  int prev_cycle=0;
  int prev_frame=0;
  int base_frame=0;

  for(int n=0 ; n<NFRAMES ; n++) {
    FlyCapture2::Image image;
    error = camera.RetrieveBuffer( &image );
    FlyCapture2Util::check_error(error);

    if(writer.is_initialized()) {
      int id = writer.get_available_slot_id();
      assert(image.GetDataSize() == (unsigned int)MEMSIZE);
      my_memcpy<char>(writer.buf(id), image.GetData(), MEMSIZE);
      writer.write(id, n);

        // TMP::save -------------------------------------------------------------
        FlyCapture2::Image rawImage;
        // Retrieve an image
        FlyCapture2::Image convertedImage;
        error = image.Convert( FlyCapture2::PIXEL_FORMAT_MONO8, &convertedImage );
        error = convertedImage.Save( OUT_FNAME.c_str() );
        // save ------------------------------------------------------------------
    }

    FlyCapture2::ImageMetadata meta = image.GetMetadata();
    FlyCapture2::TimeStamp timestamp = image.GetTimeStamp();

    if(n>0) {
      int diff_cycle = ((timestamp.cycleCount - prev_cycle + CYCLE_MAX) % CYCLE_MAX) - CYCLE_AVE;

      if(diff_cycle > CYCLE_TOL || diff_cycle < -CYCLE_TOL) {
        printf("\nSN%u too much time diff at n=%d (%d - %d = %d > %d +- %d).\n\n", CAMID, n, timestamp.cycleCount, prev_cycle, (timestamp.cycleCount - prev_cycle + CYCLE_MAX) % CYCLE_MAX, CYCLE_AVE, CYCLE_TOL);
      }

      if(meta.embeddedFrameCounter - prev_frame != 1) {
        printf("\nSN%u frame drop at n=%d (%d - %d = %d != 1).\n\n", CAMID, n, meta.embeddedFrameCounter, prev_frame, meta.embeddedFrameCounter - prev_frame);
      }

    } else {
      base_frame = meta.embeddedFrameCounter;
    }

    printf( "SN%u[%d] : TimeStamp [%03d %04d], Counter %d (+%d)\n",
            CAMID,
            n,
            timestamp.cycleSeconds,
            timestamp.cycleCount,
            meta.embeddedFrameCounter,
            meta.embeddedFrameCounter - base_frame);

    prev_cycle = timestamp.cycleCount;
    prev_frame = meta.embeddedFrameCounter;

  }

  camera.StopCapture();

  // Turn off trigger out anyway
  // FlyCapture2Util::set_trigger_out(&camera, false);

  camera.Disconnect();

  return 0;
}
