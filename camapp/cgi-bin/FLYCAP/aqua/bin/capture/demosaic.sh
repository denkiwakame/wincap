#!/bin/bash

if [ $# != 5 ]; then
    echo
    echo $0 RAW-file frame# width height OUT.pgm
    echo
    exit
fi

RAW=$1
FRAME=$2
WIDTH=$3
HEIGHT=$4
OUT=$5

SZ=$(( $WIDTH * $HEIGHT))

cat<<EOD > $OUT
P5
$WIDTH $HEIGHT
255
EOD

dd if=$RAW of=$OUT conv=notrunc oflag=append bs=$SZ skip=$FRAME count=1

