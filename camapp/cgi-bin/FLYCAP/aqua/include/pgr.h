#ifndef LIBCAPTURE_PGR_H
#define LIBCAPTURE_PGR_H

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

extern "C" {
#include <dc1394/dc1394.h>
}
#include <sys/time.h>
#include <arpa/inet.h>

#define PGR_FRAMECOUNT_BASE (3)

#define PGR_SIMPLE_FRAMECOUNT_MAX (16)
#define PGR_TIMESTAMP_MAX (128000000)
#define PGR_FRAME_INFO_REGISTER (0x12f8)
#define PGR_FIRMWARE_DESCRIPTION (0x1f68) // 1f7c
#define PGR_FIRMWARE_DESCRIPTION_LAST (0x1f7c)
#define PGR_SERIAL_REGISTER (0x1f20)

inline uint32_t pgr_serial_number(dc1394camera_t * camera) {
  dc1394error_t err;
  uint32_t val;
  err = dc1394_get_control_register( camera, PGR_SERIAL_REGISTER, &val );
  return ntohl(val);
}

inline std::string pgr_firmware_description(dc1394camera_t * camera) {
  dc1394error_t err;
  const int n = (PGR_FIRMWARE_DESCRIPTION_LAST-PGR_FIRMWARE_DESCRIPTION)/4+1;
  uint32_t desc[n];
  for(int i=0 ; i<n ; i++) {
    err = dc1394_get_control_register( camera, PGR_FIRMWARE_DESCRIPTION+4*i, desc+i );
    desc[i] = ntohl(*((u_int32_t *)(desc+i)));
  }

  std::string d((char*)desc, (char*)(desc+n));
  fprintf(stderr, "Firmware = %s, Serial = %" PRIu32 "\n", d.c_str(), pgr_serial_number(camera));
  return d;
}


inline dc1394error_t pgr_frameinfo_on( dc1394camera_t* camera ) {
  dc1394error_t err;
  //err = dc1394_set_control_register( camera, FRAME_INFO_REGISTER, 0x80000001 );
  err = dc1394_set_control_register( camera, PGR_FRAME_INFO_REGISTER, 0x800003ff );
  return err;
}

inline dc1394error_t pgr_frameinfo_off( dc1394camera_t* camera ) {
  dc1394error_t err;
  //err = dc1394_set_control_register( camera, FRAME_INFO_REGISTER, 0x80000001 );
  err = dc1394_set_control_register( camera, PGR_FRAME_INFO_REGISTER, 0x80000000 );
  return err;
}

inline unsigned int pgr_get_timestamp( unsigned char * buffer ) {
   unsigned int timestamp;
   //timestamp = buffer[0] << 24;
   //timestamp += buffer[1] << 16;
   //timestamp += buffer[2] << 8;
   //timestamp += buffer[3] << 0;

   // 先頭 4 byte を得る
   u_int32_t v = ntohl(*((u_int32_t *)buffer));

   timestamp = ((v >> 25) & 0x7f) * 1000000 + ((v >> 12) & 0x1fff) * 125 +
     (int)(((v) & 0xfff) * (1.0/24.576));

   return timestamp;
}

inline int64_t pgr_get_framecount( unsigned char * buffer ) {
  // 先頭 24-27 byte を得る
  u_int32_t v = ntohl(*((u_int32_t *)(buffer+24)));

  return v;
}

template<typename T1, typename T2>
inline void pgr_get_timestamp_and_simple_framecount( unsigned char * buffer, T1 * timestamp, T2 * four_bit_framecount ) {
   // 先頭 4 byte を得る
   u_int32_t v = ntohl(*((u_int32_t *)buffer));

   *timestamp = ((v >> 25) & 0x7f) * 1000000 + ((v >> 12) & 0x1fff) * 125 +
     (int)(((v) & 0xff0) * (1.0/24.576));

   *four_bit_framecount = v & 0xf;
}

#endif
