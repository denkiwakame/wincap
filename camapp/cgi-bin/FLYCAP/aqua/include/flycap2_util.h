#ifndef FLYCAP2_UTIL_H
#define FLYCAP2_UTIL_H


#include <flycapture/FlyCapture2.h>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstdarg>

namespace FlyCapture2Util {
  inline void print_error( FlyCapture2::Error error ) {
    error.PrintErrorTrace();
  }

  inline void __attribute__((format(printf,1,2))) debug( const char * msg, ... ) {
    va_list ap;
    const int sz = 65536;
    char buf[sz];
    
    va_start(ap, msg);
    int s = vsnprintf(buf, sz, msg, ap);
    va_end(ap);
    
    assert( s < sz );

    fprintf(stderr, "[DEBUG] %s", buf);
  }

  inline void __attribute__((format(printf,1,2))) warn( const char * msg, ... ) {
    va_list ap;
    const int sz = 65536;
    char buf[sz];
    
    va_start(ap, msg);
    int s = vsnprintf(buf, sz, msg, ap);
    va_end(ap);
    
    assert( s < sz );

    fprintf(stderr, "[WARN] %s", buf);
  }

  inline void __attribute__((format(printf,3,4))) check_error( FlyCapture2::Error error, int exit_code=1, const char * msg="[ERROR]\n", ... ) {
    if (error != FlyCapture2::PGRERROR_OK) {
      print_error( error );

      va_list ap;
      const int sz = 65536;
      char buf[sz];
      
      va_start(ap, msg);
      int s = vsnprintf(buf, sz, msg, ap);
      va_end(ap);
      
      assert( s < sz );

      std::cerr << buf;
      exit(exit_code);
    }
  }

  inline std::string build_info() {
    FlyCapture2::FC2Version fc2Version;
    FlyCapture2::Utilities::GetLibraryVersion( &fc2Version );
    char buf[1024];
    snprintf(buf, sizeof(buf),
             "FlyCapture2 library version: %d.%d.%d.%d\n"
             "Application build date: %s %s\n\n",
             fc2Version.major, fc2Version.minor, fc2Version.type, fc2Version.build,
             __DATE__, __TIME__ 
             );

    return std::string(buf);
  }

  inline std::string to_string( FlyCapture2::CameraInfo * pCamInfo ) {
    char buf[1024];
    snprintf(buf, sizeof(buf),
             "\n*** CAMERA INFORMATION ***\n"
             "Serial number - %u\n"
             "Camera model - %s\n"
             "Camera vendor - %s\n"
             "Sensor - %s\n"
             "Resolution - %s\n"
             "Firmware version - %s\n"
             "Firmware build time - %s\n\n",
             pCamInfo->serialNumber,
             pCamInfo->modelName,
             pCamInfo->vendorName,
             pCamInfo->sensorInfo,
             pCamInfo->sensorResolution,
             pCamInfo->firmwareVersion,
             pCamInfo->firmwareBuildTime );

    return std::string(buf);
  }

  inline std::string get_model( FlyCapture2::Camera * camera ) {
    FlyCapture2::Error error;
    FlyCapture2::CameraInfo camerainfo;
    error = camera->GetCameraInfo(&camerainfo);
    FlyCapture2Util::check_error(error);

    return std::string(camerainfo.modelName);
  }

  inline std::string get_firmware_version( FlyCapture2::Camera * camera ) {
    FlyCapture2::Error error;
    FlyCapture2::CameraInfo camerainfo;
    error = camera->GetCameraInfo(&camerainfo);
    FlyCapture2Util::check_error(error);

    return std::string(camerainfo.firmwareVersion);
  }

  inline void check_firmware( FlyCapture2::Camera * camera ) {
    std::string model = get_model(camera);
    std::string firmware = get_firmware_version(camera);

    if(model.find("Chameleon CMLN") != std::string::npos) {
      const std::string LATEST_FIRMWARE = "1.12.3.0";
      if(firmware == LATEST_FIRMWARE) {
        debug("%s is using the latest firmware %s.\n", model.c_str(), LATEST_FIRMWARE.c_str());
      } else {
        warn("%s is NOT using the latest firmware %s (was %s).\n", model.c_str(), LATEST_FIRMWARE.c_str(), firmware.c_str());
      }
    } else {
      warn("Unknown camera %s. Skipping firmware version check.\n", model.c_str());
    }
  }

  inline void enable_info_embedding( FlyCapture2::Camera * camera ) {
    FlyCapture2::EmbeddedImageInfo info;
    FlyCapture2::Error error;

    error = camera->GetEmbeddedImageInfo( &info );
    FlyCapture2Util::check_error(error);

    info.timestamp.onOff = true;
    info.gain.onOff = true;
    info.shutter.onOff = true;
    info.brightness.onOff = true;
    info.exposure.onOff = true;
    info.whiteBalance.onOff = true;
    info.frameCounter.onOff = true;
    info.strobePattern.onOff = true;
    info.GPIOPinState.onOff = true;
    info.ROIPosition.onOff = true;

    error = camera->SetEmbeddedImageInfo( &info );
    FlyCapture2Util::check_error(error);
  }

  inline std::string to_string(FlyCapture2::PropertyType p) {
    using namespace FlyCapture2;
    switch(p) {
    case BRIGHTNESS: return "Brightness";
    case AUTO_EXPOSURE: return "Auto exposure";
    case SHARPNESS: return "Sharpness";
    case WHITE_BALANCE: return "White balance";
    case HUE: return "Hue";
    case SATURATION: return "Saturation";
    case GAMMA: return "Gamma";
    case IRIS: return "Iris";
    case FOCUS: return "Focus";
    case ZOOM: return "Zoom";
    case PAN: return "Pan";
    case TILT: return "Tilt";
    case SHUTTER: return "Shutter";
    case GAIN: return "Gain";
    case TRIGGER_MODE: return "Trigger mode";
    case TRIGGER_DELAY: return "Trigger delay";
    case FRAME_RATE: return "Frame rate";
    case TEMPERATURE: return "Temperature";
    default: return "Unspecified property type";
    }
  }

  template<int prop>
  struct enable_abs_mode_prop {
    explicit enable_abs_mode_prop( FlyCapture2::Camera * camera, bool on_off ) {
      FlyCapture2::Error error;
      FlyCapture2::Property p((FlyCapture2::PropertyType)prop);
      FlyCapture2::PropertyInfo pi((FlyCapture2::PropertyType)prop);

      error = camera->GetPropertyInfo(&pi);
      FlyCapture2Util::check_error(error);

      error = camera->GetProperty(&p);
      FlyCapture2Util::check_error(error);

      if( !pi.present ) {
        debug("%s is not supported. Skip.\n", to_string(p.type).c_str());
        return;
      }

      p.absControl = false;
      if( on_off ) {
        if( pi.absValSupported ) {
          p.absControl = true;
        } else {
          debug("%s does not support ABS.\n", to_string(p.type).c_str());
        }
      }

      p.autoManualMode = false;
      p.onePush = false;
      p.onOff = true;

      error = camera->SetProperty(&p);
      FlyCapture2Util::check_error(error);
    }
  };

  typedef enable_abs_mode_prop<FlyCapture2::BRIGHTNESS> enable_abs_mode_brightness;
  typedef enable_abs_mode_prop<FlyCapture2::AUTO_EXPOSURE> enable_abs_mode_auto_exposure;
  typedef enable_abs_mode_prop<FlyCapture2::SHARPNESS> enable_abs_mode_sharpness;
  typedef enable_abs_mode_prop<FlyCapture2::WHITE_BALANCE> enable_abs_mode_white_balance;
  typedef enable_abs_mode_prop<FlyCapture2::HUE> enable_abs_mode_hue;
  typedef enable_abs_mode_prop<FlyCapture2::SATURATION> enable_abs_mode_saturation;
  typedef enable_abs_mode_prop<FlyCapture2::GAMMA> enable_abs_mode_gamma;
  typedef enable_abs_mode_prop<FlyCapture2::IRIS> enable_abs_mode_iris;
  typedef enable_abs_mode_prop<FlyCapture2::FOCUS> enable_abs_mode_focus;
  typedef enable_abs_mode_prop<FlyCapture2::ZOOM> enable_abs_mode_zoom;
  typedef enable_abs_mode_prop<FlyCapture2::PAN> enable_abs_mode_pan;
  typedef enable_abs_mode_prop<FlyCapture2::TILT> enable_abs_mode_tilt;
  typedef enable_abs_mode_prop<FlyCapture2::SHUTTER> enable_abs_mode_shutter;
  typedef enable_abs_mode_prop<FlyCapture2::GAIN> enable_abs_mode_gain;
  typedef enable_abs_mode_prop<FlyCapture2::FRAME_RATE> enable_abs_mode_frame_rate;
  typedef enable_abs_mode_prop<FlyCapture2::TEMPERATURE> enable_abs_mode_temperature;

  inline void enable_abs_mode( FlyCapture2::Camera * camera, bool on_off ) {
    enable_abs_mode_brightness(camera, on_off);
    enable_abs_mode_auto_exposure(camera, on_off);
    enable_abs_mode_sharpness(camera, on_off);
    enable_abs_mode_white_balance(camera, on_off);
    enable_abs_mode_hue(camera, on_off);
    enable_abs_mode_saturation(camera, on_off);
    enable_abs_mode_gamma(camera, on_off);
    enable_abs_mode_iris(camera, on_off);
    enable_abs_mode_focus(camera, on_off);
    enable_abs_mode_zoom(camera, on_off);
    enable_abs_mode_pan(camera, on_off);
    enable_abs_mode_tilt(camera, on_off);
    enable_abs_mode_shutter(camera, on_off);
    enable_abs_mode_gain(camera, on_off);
    enable_abs_mode_frame_rate(camera, on_off);
    enable_abs_mode_temperature(camera, on_off);
  }

  template<int prop>
  struct set_abs_prop {
    set_abs_prop( FlyCapture2::Camera * camera, float val ) {
      FlyCapture2::Error error;
      FlyCapture2::Property p((FlyCapture2::PropertyType)prop);

      error = camera->GetProperty(&p);
      FlyCapture2Util::check_error(error);

      p.absControl = true;
      p.onOff = true;
      p.absValue = val;

      error = camera->SetProperty(&p);
      FlyCapture2Util::check_error(error);
    }
  };

  typedef set_abs_prop<FlyCapture2::SHUTTER> set_abs_shutter;
  typedef set_abs_prop<FlyCapture2::GAMMA> set_abs_gamma;
  typedef set_abs_prop<FlyCapture2::GAIN> set_abs_gain;
  typedef set_abs_prop<FlyCapture2::FRAME_RATE> set_abs_frame_rate;


  template<int prop>
  struct set_man_prop_base {
    set_man_prop_base( FlyCapture2::Camera * camera, unsigned int valA, unsigned int valB ) {
      FlyCapture2::Error error;
      FlyCapture2::Property p((FlyCapture2::PropertyType)prop);

      error = camera->GetProperty(&p);
      FlyCapture2Util::check_error(error);

      p.absControl = false;
      p.onOff = true;
      p.valueA = valA;

      if(p.type == FlyCapture2::WHITE_BALANCE) {
        p.valueB = valB;
      }

      error = camera->SetProperty(&p);
      FlyCapture2Util::check_error(error);
    }
  };

  template<int prop>
  struct set_man_prop : public set_man_prop_base<prop> {
    set_man_prop( FlyCapture2::Camera * camera, unsigned int valA ) : set_man_prop_base<prop>(camera, valA, 0) {}
  };

  // function-objects of 2 args
  typedef set_man_prop<FlyCapture2::BRIGHTNESS> set_man_brightness;
  typedef set_man_prop<FlyCapture2::AUTO_EXPOSURE> set_man_auto_exposure;
  typedef set_man_prop<FlyCapture2::SHARPNESS> set_man_sharpness;
  typedef set_man_prop<FlyCapture2::HUE> set_man_hue;
  typedef set_man_prop<FlyCapture2::SATURATION> set_man_saturation;
  typedef set_man_prop<FlyCapture2::GAMMA> set_man_gamma;
  typedef set_man_prop<FlyCapture2::IRIS> set_man_iris;
  typedef set_man_prop<FlyCapture2::FOCUS> set_man_focus;
  typedef set_man_prop<FlyCapture2::ZOOM> set_man_zoom;
  typedef set_man_prop<FlyCapture2::PAN> set_man_pan;
  typedef set_man_prop<FlyCapture2::TILT> set_man_tilt;
  typedef set_man_prop<FlyCapture2::SHUTTER> set_man_shutter;
  typedef set_man_prop<FlyCapture2::GAIN> set_man_gain;
  typedef set_man_prop<FlyCapture2::FRAME_RATE> set_man_frame_rate;
  typedef set_man_prop<FlyCapture2::TEMPERATURE> set_man_temperature;

  // function-objects of 3 args
  typedef set_man_prop_base<FlyCapture2::WHITE_BALANCE> set_man_white_balance;

  inline void set_trigger_in(FlyCapture2::Camera * camera, bool on_off) {
    FlyCapture2::Error error;
    FlyCapture2::TriggerMode mode;
    error = camera->GetTriggerMode(&mode);
    FlyCapture2Util::check_error(error);

    mode.onOff = on_off;
    mode.polarity = 0;
    mode.source = 0;
    mode.mode = 14;

    error = camera->SetTriggerMode(&mode);
    FlyCapture2Util::check_error(error);
  }

  /** 
   * Set camera to send out trigger pulse from GPIO
   * 
   * @param camera 
   * @param on_off 
   * @param sync_to_trigger_in
   * @param pin
   */
  inline void set_trigger_out(FlyCapture2::Camera * camera, bool on_off, bool sync_to_trigger_in=true, unsigned int pin=3) {
    FlyCapture2::Error error;
    FlyCapture2::StrobeControl sctrl;

    sctrl.source = pin;
    sctrl.onOff = on_off;
    sctrl.polarity = 0;
    sctrl.delay = 0;
    sctrl.duration = 0; // duration 0 => duty=exposure

    error = camera->SetStrobe(&sctrl, false);
    FlyCapture2Util::check_error(error);


    // set GPIO_XTRA[0] = 1, to let the strobe output be relative to the time of a trigger.
    // see Point Grey Camera Register Reference, Section "GPIO_XTRA: 1104h".
    unsigned int v;

    // read the current GPIO_XTRA register
    error = camera->ReadRegister(0x1104U, &v);
    FlyCapture2Util::check_error(error);

    // set the bit-0 (MSB) be 1. be sure of the endian.
    v |= 0x01000000U;

    // write GPIO_XTRA
    error = camera->WriteRegister(0x1104U, v);
    FlyCapture2Util::check_error(error);

    // check
    error = camera->ReadRegister(0x1104U, &v);
    FlyCapture2Util::check_error(error);
    if(v & 0x01000000U != 0x01000000U) {
      warn("Cannot setup GPIO_XTRA register (val=%x)\n", v);
    }

  }

}

#endif //FLYCAP2_UTIL_H
