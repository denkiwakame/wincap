#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>

std::string gen_fpath(int i, std::string ext){
    std::stringstream ss;
    ss << "./data/chessboard/";
    if (i < 100) ss << "0";
    if (i < 10) ss << "0";
    ss << i << ext;
    return ss.str();
}
std::string gen_fpath_tmp(int i, std::string ext){
    std::stringstream ss;
    ss << "./data/chessboard/samples/";
    ss << i << ext;
    return ss.str();
}
int toInt(std::string s) { int r = 0; std::istringstream ss(s); ss >> r; return r; }

int main( int argc, char** argv )
{
	const std::string window_name_original_img = "Original Image";
	const std::string window_name_undistorted_img = "Undistorted Image";

    assert(argc == 5 && "Usage: ./incalib chessboard_col chessboard_row unit_size chessnum_of_images\nExample: ./incalib 10 7 10 20");
    const int chessboard_col = toInt(std::string(argv[1]));
    const int chessboard_row = toInt(std::string(argv[2]));
    const int unit_size = toInt(std::string(argv[3]));
	const int number_of_chessboard_imgs = toInt(std::string(argv[4]));

	// calibration
	cv::vector<cv::Mat> chessboard_imgs;
    // cv::Size(width, height) chess board crosspoints num
	const cv::Size chessboard_size( chessboard_col, chessboard_row );

//	cv::TermCriteria criteria( CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.001 );

	// intrinsic params
	cv::Mat intrinsic_param_matrix;
	cv::Mat	distortion_coeffs;
    // extrinsic params (unique to each image)
	cv::vector<cv::Mat>	rotation_vectors;
	cv::vector<cv::Mat>	translation_vectors;

    // load chessboard_img
	for( int i = 0; i < number_of_chessboard_imgs; i++ ) {
        std::string fname = gen_fpath(i,".pgm");
        //std::string fname = gen_fpath_tmp(i,".bmp");
        cv::Mat load_img = cv::imread( fname, 0);
        if (load_img.empty()) { std::cerr << "cannot load img" << std::endl; return -1; }

		chessboard_imgs.push_back( load_img ); // gray scale
		std::cerr << "Load checker image: " << fname << std::endl;
	}

    // find chessboard crosspoints and save them into image_points
	cv::vector< cv::vector<cv::Point2f> > image_points(number_of_chessboard_imgs);
	cv::namedWindow( window_name_original_img, CV_WINDOW_AUTOSIZE );
	for( int i = 0; i < number_of_chessboard_imgs; i++ ) {
		std::cerr << "Find corners from image " << i;
        cv::vector<cv::Point2f> image_point;
		if( cv::findChessboardCorners( chessboard_imgs[i], chessboard_size, image_points[i] ) ) {
			std::cerr << " ... All corners found." << std::endl;
            // render image_points
			cv::drawChessboardCorners( chessboard_imgs[i], chessboard_size, ( cv::Mat )( image_points[i] ), true );
			cv::imshow( window_name_original_img, chessboard_imgs[i] );
			cv::waitKey(100);
		} else {
			std::cerr << " ... at least 1 corner not found." << std::endl;
			cv::waitKey(100);
			//return -1;
		}
	}

    // world points corresponds to chessboard points
	cv::vector< cv::vector<cv::Point3f> > world_points(number_of_chessboard_imgs);
	// save world points
	for( uint i = 0; i < number_of_chessboard_imgs; i++ ) {
		for( int j = 0 ; j < chessboard_size.area(); j++ ) { // row*col
			world_points[i].push_back( cv::Point3f( static_cast<float>( j % chessboard_size.width * unit_size ),
						                            static_cast<float>( j / chessboard_size.width * unit_size ),
						                            0.0 ));
		}
	}

    // incalib
    assert(world_points.size()==image_points.size());
    std::cerr << "Calcurating..." << std::endl;
	cv::calibrateCamera( world_points, image_points, chessboard_imgs[0].size(), intrinsic_param_matrix, distortion_coeffs, rotation_vectors, translation_vectors);
	std::cerr << "Camera parameters have been estimated" << std::endl << std::endl;

	// undistortion
	std::cerr << "Undistorted images" << std::endl;
	cv::Mat	undistorted_img;
	cv::namedWindow( window_name_undistorted_img );
	for( int i = 0; i < number_of_chessboard_imgs; i++ ) {
		cv::undistort( chessboard_imgs[i], undistorted_img, intrinsic_param_matrix, distortion_coeffs);
		cv::imshow( window_name_undistorted_img, undistorted_img );
		cv::imshow( window_name_undistorted_img, chessboard_imgs[i] );
		cv::waitKey( 100 );
	}

    // filio::output
    system("mkdir data");
    cv::FileStorage cvfs("./data/camera.xml", CV_STORAGE_WRITE);
    cv::write(cvfs,"intrinsicMat", intrinsic_param_matrix);
    cv::write(cvfs,"distCoeffs", distortion_coeffs);

    // fileio::input
    /*
    cv::FileStorage cvfs("./data/camera.xml", CV_STORAGE_READ);
    cv::FileNode node(cvfs.fs, NULL);
    cv::Mat in_mat, dist_coeff;
    cv::read(node["intrinsicMat"], in_mat);
    cv::read(node["distCoeffs"], dist_coeff);
    std::cerr << ma << std::endl;
    std::cerr << mb << std::endl;
    */

	return 0;
}
