#!/usr/bin/python
import sys, os
import json, cgi

from xml.etree import ElementTree

query_params  = cgi.FieldStorage()
xml_fname         = str(query_params['xml_fname'].value) if query_params.has_key('xml_fname') else 'camera.xml'

CAMERA_XML_FPATH = "./data/" + xml_fname

if os.path.exists(CAMERA_XML_FPATH):
    os.system("rm -rf " + CAMERA_XML_FPATH)

chessboard_col = str(query_params['chessboard_col'].value) if query_params.has_key('chessboard_col') else 0
chessboard_row = str(query_params['chessboard_row'].value) if query_params.has_key('chessboard_row') else 0
unit_size = str(query_params['unit_size'].value) if query_params.has_key('unit_size') else 0
num_of_images = str(query_params['num_of_images'].value) if query_params.has_key('num_of_images') else 0

cmd = "./cgi-bin/incalib/incalib " + chessboard_col + " " + chessboard_row + " " + unit_size + " " + num_of_images + " .png " + CAMERA_XML_FPATH
sys.stderr.write(cmd)
os.system(cmd)

if os.path.exists(CAMERA_XML_FPATH):

    res = json.dumps({
                    'status': "SUCCESS",
                    })
else:
    res = json.dumps({
                    'status': "FAIL",
                    })

print "Content-type: application/json\n"
print res
