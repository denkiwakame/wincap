$(function(){

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');

var AB = {
    W:  2048,
    H:  1024,
    aryW:   new Array(this.H),
    aryH:   new Array(this.W),
};
for (var i in AB.aryW) {
    AB.aryW[i] = 0;
}
for (var i in AB.aryH) {
    AB.aryH[i] = 0;
}

if(!canvas || !canvas.getContext){
    alert('this browse does not support canvas');
}
canvas.width = AB.W;
canvas.height = AB.H;

/*
function resizeCanvas() {
    console.log("canvas resized");
    var w = window.innerWidth;
    var h = window.innerHeight;
    if (w % 2 != 0) {
        w = w-1;
    }
    if (h % 2 != 0){
        h = h-1;
    }
    canvas.width = w;
    canvas.height = h;
};
*/

function setAryVal(ary, begin, end, val){ // 1 or 0
    for(var i=begin; i<end; i++){
        ary[i] = (ary[i] << 1) + val;
    }
}

function toBinary(num) {
    var binaryNum = "";
    while(num) {
        binaryNum = (num & 1) + binaryNum;
        num >>= 1; // num >>> ignores sign
    }
    return binaryNum;
}

// Active Background Display Drawing ---------------------------------
function splitV (interval) {
    var d = new $.Deferred;
    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawV(0, canvas.width, canvas.height, interval, d);
    return d.promise();
}

function splitH (interval) {
    var d = new $.Deferred;
    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawH(0, canvas.width, canvas.height, interval, d);
    return d.promise();
}

function drawV(topLeftX, width, height, interval, d) {
    ctx.fillStyle = 'rgb(0,0,0)';
    ctx.fillRect(topLeftX, 0, width/2, height);
    setAryVal(AB.aryW, topLeftX, topLeftX+width/2, 0);

    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(topLeftX + width/2, 0, width/2, height);
    setAryVal(AB.aryW, topLeftX + width/2, topLeftX + width, 1);

    if (width/2 > 1) {
        setTimeout(function(){
            drawV(topLeftX, width/2, height, interval, d);
        }, interval);
        setTimeout(function(){
            drawV(topLeftX + width/2, width/2, height, interval, d);
        }, interval);
    } else if (!d.isResolved) {
        d.resolve();
    }
};

function drawH(topY, width, height, interval, d) {
    ctx.fillStyle = 'rgb(0,0,0)';
    ctx.fillRect(0, topY, width, height/2);
    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(0, topY + height/2, width, height/2);

    if (height/2 > 1) {
        setTimeout(function(){
            drawH(topY, width, height/2, interval, d);
        }, interval);
        setTimeout(function(){
            drawH(topY + height/2, width, height/2, interval, d);
        }, interval);
    } else if (!d.isResolved) {
        d.resolve();
    }
};

$("#startbtn").on('click', function(){
    canvas = document.getElementById("canvas");
    if (canvas.webkitRequestFullScreen) {
        canvas.webkitRequestFullScreen();
    }
    else if (canvas.mozRequestFullScreen) {
        canvas.mozRequestFullScreen();
    }
    // IE
    else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
    }
    else {
        alert("use google chrome or mozilla firefox");
    }
    // TODO changes the initial image to marker
    // initial image
    ctx.fillStyle = 'rgb(0,255,0)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    var aryWOut = document.getElementById("outV");
    var setup = $("#setup").val();
    var interval = $("#interval").val();

    setTimeout(function(){
        splitV(interval).done(function(){
            setTimeout(function(){
                splitH(interval).done(function(){
                    console.log(AB.aryW);
                    console.log(AB.aryW.length);
                    for (i=0;i<AB.aryW.length; i++){
                        var binaryNum = toBinary(AB.aryW[i]);
                        aryWOut.value += String(i) + ", " + String(AB.aryW[i]) + ", " + String(binaryNum) + "\n";
                    }

                    setTimeout(function(){
                        // EXIT FULLSCREEN
                        if(document.webkitExitFullscreen){
                            document.webkitExitFullscreen();
                        }
                    },interval);
                });
            },interval);
        });
    },setup);
});

$("#calibbtn").on('click', function(){
    canvas = document.getElementById("canvas");
    if (canvas.webkitRequestFullScreen) {
        canvas.webkitRequestFullScreen();
    }
    else if (canvas.mozRequestFullScreen) {
        canvas.mozRequestFullScreen();
    }
    else {
        alert("use google chrome or mozilla firefox");
    }
    // TODO changes the initial image to marker
    // initial image
    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

/*
    $("#canvas").keydown(function(e){
        console.log(e.keyCode);
        switch(e.keyCode) {
            case 66:
                ctx.fillStyle = 'rgb(0,0,0)';
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                break;
            case 87:
                ctx.fillStyle = 'rgb(255,255,255)';
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                break;
            case 13: // enter
                break;
        }
        return false;
    });
*/
    var setup = $("#setup").val();
    setTimeout(function(){
        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }, setup);

});


});
