$(function(){

    // util func
    Util = {
        getParams: function(){
            var params_str = location.search.slice(1); // delete "?"
            if (!params_str) return {};

            var params_ary = decodeURIComponent(params_str).split("&");

            var params = {};
            for(var i=0; i<params_ary.length; i++){
                var key_val = params_ary[i].split("=");
                params[key_val[0]] = key_val[1].split("/")[0];
            }
            return params;
        },
        // XXX イベントにつけないと作動しない
        onFullscreen: function(){
            canvas = document.getElementById("canvas");
            if (canvas.webkitRequestFullscreen){
                canvas.webkitRequestFullscreen();
            } else if (canvas.mozRequestFullScreen){
                canvas.mozRequestFullScreen();
            } else {
                alert("use google chrome or mozzila fire fox");
            }
        },
        // XXX Exit するときはdocument要素でないとX
        offFullscreen: function(){
            if (document.webkitExitFullscreen){
                document.webkitExitFullscreen();
            }
        },
        errLog: function(log){
            msg = "ERROR: " + log;
            console.log(msg);
        },
        alignNum: function(num){
            var num = Number(num);
            if (num < 10) return "00" + num;
            if (num < 100) return "0" + num;
            return String(num);
        },
    };

    // prepare canvas
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext('2d');
    if (!canvas || !canvas.getContext) alert('this browser does not support canvas');

    var GrayCode = {
        width: 2048,
        height: 1024,
        xSplitNum: Math.log(2048) / Math.log(2),
        ySplitNum: Math.log(1024) / Math.log(2),
        fill: function(col){
            if (col){
                ctx.fillStyle = 'rgb(255,255,255)';
            } else {
                ctx.fillStyle = 'rgb(0,0,0)';
            }
            ctx.fillRect(0,0,canvas.width,canvas.height);
        },
        _draw: function(x1,y1,w1,h1,x2,y2,w2,h2){
            ctx.fillStyle = 'rgb(0,0,0)';
            ctx.fillRect(x1, y1, w1, h1);
            ctx.fillStyle = 'rgb(255,255,255)';
            ctx.fillRect(x2, y2, w2, h2);
        },
        _drawGrayCode: function(x,y,w,h,XorY,bit,isAnti){
            var gc = ( XorY == 'X' ) ? x ^ (x >> 1) : y ^ (y >> 1);

            if(!isAnti){ // NORMAL
                ctx.fillStyle = gc & (1 << (bit-1)) ? 'rgb(255,255,255)' : 'rgb(0,0,0)';
            } else { // anti-pattern
                console.log('anti pattern!');
                ctx.fillStyle = gc & (1 << (bit-1)) ? 'rgb(0,0,0)' : 'rgb(255,255,255)';
            }
            ctx.fillRect(x, y, w, h);
        },
        splitY: function(y, width, height, num, target_num, isAnti) { // ===
            if (target_num > num) {
                this.splitY(y,          width, height/2, num+1, target_num, isAnti);
                this.splitY(y+height/2, width, height/2, num+1, target_num, isAnti);
            } else {
                //GrayCode._draw(0, y, width, height/2, 0, y+height/2, width, height/2);
                GrayCode._drawGrayCode(0,   y,          width,  height/2,   'Y', GrayCode.ySplitNum - target_num, isAnti);
                GrayCode._drawGrayCode(0,   y+height/2, width,  height/2,   'Y', GrayCode.ySplitNum - target_num, isAnti);
            }
        },
        splitX: function(x, width, height, num, target_num, isAnti) { // ||
            if (target_num > num) {
                this.splitX(x,          width/2, height, num+1, target_num, isAnti);
                this.splitX(x+width/2,  width/2, height, num+1, target_num, isAnti);
            } else {
                //GrayCode._draw(x, 0, width/2, height, x+width/2, 0, width/2, height);
                GrayCode._drawGrayCode(x,           0,  width/2,    height,   'X', GrayCode.xSplitNum - target_num, isAnti);
                GrayCode._drawGrayCode(x+width/2,   0,  width/2,    height,   'X', GrayCode.xSplitNum - target_num, isAnti);
            }
        },
        getImageData: function(){
            try {
                var imageData = canvas.toDataURL("image/png");
            } catch(e) {
                var imageData = "fails";
            }
            return imageData;
        },
    };

    // TODO
    var shutterSpeeds = {};
    var mode = { 'blink': 0, 'splitX': 1, 'splitY': 2, 'antiSplitX': 3, 'antiSplitY':4,  'END': 5 };
    var Sync = {
        cam         : 0,
        set         : 1,
        capture     : 0,
        pattern_num : 0,
        //shutter_speeds: [ 1, 10, 50, 100 ], // TODO
        shutter_speeds: [ 1, 10, 50 ], // TODO
        mode: mode['blink'], // init FIXME フルスクリーンを解除しますか？の表示があって邪魔みたいな理由
        //mode: mode['splitX'], // init
        setUp: function(param_hash){
            GrayCode.width = Number( $("#gc-width option:selected").val() );
            GrayCode.height = Number( $("#gc-height option:selected").val() );
            GrayCode.xSplitNum = Math.log(GrayCode.width) / Math.log(2);
            GrayCode.ySplitNum = Math.log(GrayCode.height) / Math.log(2);
            if (!canvas) { Util.errLog('canvas yet set'); return; };
            canvas.width = GrayCode.width;
            canvas.height = GrayCode.height;

            Sync.cam = Number($("#gc-cam").val());
            Sync.set = Number($("#gc-set").val());
            Sync.capture = param_hash['capture'];

            Sync.shutter_speeds = $("#gc-shutter option:selected").val() == "multi" ? 1 : 0;

            Util.onFullscreen();

            Sync.drawAndCapture();
        },
        evalMode: function(){
            if (this.mode == mode['blink']) {
                this.mode = Sync.pattern_num < 2 ? mode['blink'] : mode['splitX'];

                // reset
                if(this.mode == mode['splitX']) Sync.pattern_num = 0;
            } else if (this.mode == mode['splitX']) {

                // XXX confirm -------------------------
                //TestDecoder.pushGrayCodes();
                //console.log(TestDecoder.graycodes);
                //TestDecoder.evalGrayCodes();
                // XXX confirm -------------------------

                this.mode = Sync.pattern_num < GrayCode.xSplitNum ? mode['splitX'] : mode['splitY'];

                // reset
                if(this.mode == mode['splitY']) Sync.pattern_num = 0;
            } else if (this.mode == mode['splitY']) {

                this.mode = Sync.pattern_num < GrayCode.ySplitNum ? mode['splitY'] : mode['antiSplitX'];
                if(this.mode == mode['antiSplitX']) Sync.pattern_num = 0;

            } else if (this.mode == mode['antiSplitX']){

                this.mode = Sync.pattern_num < GrayCode.xSplitNum ? mode['antiSplitX'] : mode['antiSplitY'];
                if(this.mode == mode['antiSplitY']) Sync.pattern_num = 0;

            } else if (this.mode == mode['antiSplitY']){

                this.mode = Sync.pattern_num < GrayCode.ySplitNum ? mode['antiSplitY'] : mode['END'];

            }
        },
        drawAndCapture: function(){
            this.evalMode();

            if (this.mode == mode['blink']){
                var col = Sync.pattern_num % 2 ? 0 : 1;
                GrayCode.fill(col);
            } else if (this.mode == mode['splitX']){
                GrayCode.splitX(0, canvas.width, canvas.height, 0, Sync.pattern_num, false);
            } else if (this.mode == mode['splitY']){
                GrayCode.splitY(0, canvas.width, canvas.height, 0, Sync.pattern_num, false);
            } else if (this.mode == mode['antiSplitX']){
                GrayCode.splitX(0, canvas.width, canvas.height, 0, Sync.pattern_num, true); // anti-pattern
            } else if (this.mode == mode['antiSplitY']){
                GrayCode.splitY(0, canvas.width, canvas.height, 0, Sync.pattern_num, true); // anti-pattern
            } else {
                console.log("GRAYCODE END");
                Util.offFullscreen();
                //setTimeout(function(){ location.href = "/"; }, 1000);
                return;
            }

            $.getJSON("/cgi-bin/capture.py", {
                'dir_name'      : "gray_code/",
                'set_num'       : Sync.set,
                'fname'         : Sync.mode + "_" + Util.alignNum(Sync.pattern_num),
                'fext'          : '.pgm',
                'shutter_speeds': Sync.shutter_speeds,
                'capture'       : Sync.capture,
                'cam_num'       : Sync.cam,
            }, function(data) {
                console.log(data)
                Sync.pattern_num++;
                Sync.drawAndCapture();
                //next_values = $.parseJSON(data)
                //console.log(next_values)
            });
        },
    }

    // Add events:
    // GrayCode::demo
    $("#gc-demo").on('click', function(){
        Sync.setUp({
            'capture': 0,
        });

    });
    // GrayCode::capture
    $("#gc-capture").on('click', function(){
        Sync.setUp({
            'capture': 1,
        });
    });

    TestDecoder = {
        graycodes:  [],
        init: function() { // undefinedでもjavascriptは勝手に０が詰まってうれしい
            for(var i=0; i<100; i++) { this.graycodes[i] = 0; }
        },
        getImageData: function() {
            var image = ctx.getImageData(0,0,100,100);
            return image;
        },
        getPixel: function(image,x,y) {
            var pixel = {
                'r':    image.data[(image.width*y+x)*4 + 0],
                'g':    image.data[(image.width*y+x)*4 + 1],
                'b':    image.data[(image.width*y+x)*4 + 2],
                'a':    image.data[(image.width*y+x)*4 + 3],
            };
            return pixel;
        },
        pushGrayCodes: function() {
            var image = this.getImageData();

            // CASE:splitX
            for(var i=0; i<100; i++) {
                var pixel = this.getPixel(image,i,0);
                var code = pixel['r'] == 0 ? 0 : 1;
                this.graycodes[i] = (this.graycodes[i] << 1) + code;
            }
        },
        evalGrayCodes: function() {
            for (var i=0; i<100; i++) {
                var binary = this.toBinary(this.graycodes[i]);
                var gc = this.toGrayCode(i);
                var gc_disp = this.graycodes[i];
                console.log([i, binary, gc, gc_disp]);
            }
        },
        toGrayCode: function(binary) {
            return binary ^ (binary >> 1);
        },
        toBinary: function(graycode) {
            var tmp = 0;
            tmp |= graycode;

            while (graycode > 0) {
                graycode >>= 1;
                tmp ^= graycode;
            }
            return tmp;
        },
    }




    // Preview::testcapture
    Preview = {
        persec  : 1,
        camnum  : 0,
        width   : 400,
        height  : 300,
        run     : false,
        draw    : function(camnum){
            var timestamp = new Date().getTime();
            var canvasPv = document.getElementById("canvas-preview");
            var ctxPv = canvasPv.getContext('2d');

            // TODO camnum
            var img = new Image();
            img.src = "data/preview" + String(camnum) + ".png" + '?' + timestamp;
            img.onload = function(){
                ctxPv.drawImage( img, 0, Preview.height*camnum, Preview.width, Preview.height );
            };
        },
        capture: function(){
            if (!Preview.run) return;

            $.getJSON("/cgi-bin/capture.py", {
                'persec': Preview.persec,
                'cam_num': Preview.camnum,
                'capture': 1,
            }, function(data){
                // TODO camera param返せたら便利？
                console.log(data);
                Preview.capture();
            });
        },
    };
    $("#pv-capture").on('click', function(){
        $(this).hide();
        $("#pv-capture-stop").show();
        $("#logo").hide();
        $("#js-live-preview-img").show();

        $("#canvas-preview").show();
        //$("#js-live-preview-params").show();

        Preview.run = true;
        Preview.persec = Number($("#pv-persec option:selected").val());
        Preview.camnum = Number($("#pv-camnum option:selected").val());

        var canvasPv = document.getElementById("canvas-preview");
        canvasPv.width = Preview.width;
        canvasPv.height = Preview.height*(Preview.camnum+1);
        for (var camnum=0; camnum<= Preview.camnum; camnum++) {
            // asyncな処理がある関数, forで変数共有して最悪なので外から
            setInterval("Preview.draw(" + camnum + ")", 500);
        }
        Preview.capture();
    });
    $("#pv-capture-stop").on('click', function(){
        $(this).hide();
        $("#pv-capture").show();

        Preview.run = false;

        $("#canvas-preview").hide();
        $("#js-live-preview-img").hide();
        //$("#js-live-preview-params").hide();
        $("#logo").show();
    });






    // ChessBoard::Rendering
    var ChessBoard = {
        cam: 0,
        row: 5,
        col: 8,
        unitPx: 20,
        shotCount: 0,
        shotNum: 10,
        capture: 1,
        init: function(param_hash){
            // chessboard size
            var selected = $("#cb-row-col").val();
            // camera number
            ChessBoard.cam = $("#cb-camnum").val();

            if (selected == "5x8") {
                ChessBoard.row = 5;
                ChessBoard.col = 8;
            } else if (selected == "7x10"){
                ChessBoard.row = 7;
                ChessBoard.col = 10;
            } else {
                ChessBoard.row = 11;
                ChessBoard.col = 16;
            };
            // unit
            ChessBoard.unitPx = Number($("#cb-unit-px").val());
            // capture number
            ChessBoard.shotNum = Number($("#cb-times").val());

            canvas.width = ChessBoard.unitPx * (ChessBoard.col+3);
            canvas.height = ChessBoard.unitPx * (ChessBoard.row+3);

            // shotNum
            ChessBoard.shotNum = Number($("#cb-times option:selected").val());

            // capture flag
            ChessBoard.capture = param_hash['capture'];

            // preview
            ChessBoard.preview_img = new Image();

            // toggleFullscreen
            Util.onFullscreen();
        },
        render: function(){
            ctx.fillStyle = 'rgb(255,255,255)';
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            var unitPx = ChessBoard.unitPx;
            ctx.fillStyle = 'rgb(0,0,0)';
            for (var row=0; row<=ChessBoard.row; row++) {
                for (var col=0; col<=ChessBoard.col; col++) {
                    if (row % 2 != 0  && col % 2 == 0) {
                        ctx.fillRect(unitPx + col*unitPx, unitPx + row*unitPx, unitPx, unitPx);
                    } else if (row % 2 == 0 && col % 2 != 0){
                        ctx.fillRect(unitPx + col*unitPx, unitPx + row*unitPx, unitPx, unitPx);
                    }
                }
            }
        },
        putText: function(text){
            ctx.fillStyle = 'rgb(125, 125, 125)';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = 'rgb(255,255,255)';
            ctx.font = "bold 32px sans-serif";
            ctx.fillText(text, 100, 100);
        },
        onMove: false,
        preview_img: undefined,
        /*
        preview: function(){
            if (!ChessBoard.onMove) {
                ChessBoard.incalib(); // fire next
                return;
            }

            $.getJSON("/cgi-bin/capture.py", {
                'cam_num'   : ChessBoard.cam,
                'persec'    : 0,
                'capture'   : 1,
            }, function(data){
                console.log(data);

                var timestamp = new Date().getTime();
                ChessBoard.preview_img = new Image();
                ChessBoard.preview_img.src = "data/preview.png" + '?' + timestamp;
                ChessBoard.preview_img.onload = function(){
                    ctx.drawImage(ChessBoard.preview_img, canvas.width/2-160,canvas.height/2-120,320,240);
                    ChessBoard.preview();
                };

            });
        },
        */
        incalib: function(){
            // END
            if (ChessBoard.shotCount == ChessBoard.shotNum) {
                Util.offFullscreen();

                // RUN INTRINSIC CALIBRATION
                $.getJSON('/cgi-bin/incalib.py', {
                    'xml_fname'         : 'camera' + String(ChessBoard.cam) + '.xml',
                    'chessboard_col'    : ChessBoard.col,
                    'chessboard_row'    : ChessBoard.row,
                    'unit_size'         : ChessBoard.unitPx,
                    'num_of_images'     : ChessBoard.shotNum,
                }, function(data){
                    alert(data['status']);
                });
                return;
            }

            ChessBoard.render();
            $.getJSON('/cgi-bin/capture.py', {
                'cam_num'   : ChessBoard.cam,
                'dir_name'  : 'chessboard/',
                'fname'     : Util.alignNum(ChessBoard.shotCount),
                'capture'   : ChessBoard.capture,
            }, function(data){
                console.log(data)
                ChessBoard.putText("Captured..." + ++ChessBoard.shotCount + "/" + ChessBoard.shotNum + " goNext");

                // リアルタイムプレビューしながら撮ってたけど通信調子悪くて最悪だったので没
                //ChessBoard.onMove = true;
                // ChessBoard.preview();
                //setTimeout(function(){ ChessBoard.onMove = false; }, 5000);
                ChessBoard.preview_img = new Image();
                ChessBoard.preview_img.src = data['fpath'];
                ChessBoard.preview_img.onload = function(){
                    ctx.drawImage(ChessBoard.preview_img, canvas.width/2-320,canvas.height/2-240,640,480);
                    setTimeout(function(){
                        ChessBoard.incalib(); // fire next
                    }, 1000);
                };
            });
        }
    };

    //ChessBoard::demo
    $("#cb-demo").on('click', function(){

        ChessBoard.init({ 'capture': 0 });
       // ChessBoard.incalib({ 'capture': 0 });
        ChessBoard.render();
        Util.onFullscreen();
        setTimeout("Util.offFullscreen()", 3000);
    });
    $("#cb-capture").on('click', function(){

        ChessBoard.init({ 'capture': 1 });
        ChessBoard.incalib();
    });
});
